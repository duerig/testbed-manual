#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "reservations" #:version apt-version]{Resource Reservations}

@(tb) supports @bold{reservations} that allow you to request resources ahead of
time. This can be useful for tutorials, classes, and to run larger experiments
than are typically possible on a first-come, first-served basis.

Reservations in @(tb) are @bold{per-cluster} and @bold{per-type}.  They are
tied to a @bold{project}: any experiment belonging to that project may
used the reserved nodes. Reservations are not tied to specific nodes; this
gives @(tb) maximum flexibility to do late-binding of nodes to reservations,
which makes them minimally intrusive on other users of the testbed.

@section[#:tag "reservations-guarantee"]{What Reservations Guarantee}

Having a reservation guarantees that, at minimum, the specified quantity of
nodes of the specified type will be available for use by the project during
the specified time window.

Having a reservation does @italic{not automatically start an experiment at that
time}: it ensures that the specified number of nodes are available for use by
any experiments that you or your fellow project members start.

@italic{More than one experiment} may use nodes from the reservation; for
example, a tutorial in which 40 students each will run an experiment having a
single node may be handled as a single 40-node reservation. You may also start
and terminate multiple experiments in series over the course of the
reservation: reserved nodes will not be returned to general use until your
reservation ends.

A reservation guarantees the @italic{minimum} number of nodes that will be
available; you may use more so long as they are not used by other experiments
or reservations.

Experiments run during a reservation @italic{do not automatically terminate} at
the end of the reservation; they simply become subject to the normal resource
usage policies, and, for example, may become non-extendable due to other
reservations that start after yours.

Important caveats include:

@itemlist[
    @item{Nodes can take @italic{several mintues} to be freed and reloaded
    between experiments; this means that they may take a few minutes to be
    available at the beginning of your reservation, and if you terminate an
    experiment during your reservation, they may take a few minutes to be
    usable for your next experiment.}
    @item{The reservation system cannot account for factors outside of its
    control such as @italic{hardware failures}; this many result in occasional
    failures to get the full number of nodes in exceptional circumstances.}
    @item{The reservation system ensures that enough nodes of the specified type
    are available, but does not consider other factors such as network topology,
    and so @italic{cannot guarantee that all possible experiments can be
    started}, even if they fit within the number of nodes.}
]

@section[#:tag "reservations-affect"]{How Reservations May Affect You}

Reservations held by others may affect your experiments in two ways: they
may @bold{prevent you from creating new experiments} or may @bold{prevent you
from extending existing experiments}. This ``admission control system'' is how
we ensure that nodes are available for those that have them reserved.

If there is an ongoing or upcoming reservation by another project,  you may
encounter an ``admission control'' failure when trying to create a new
experiment. This means that, although there are enough nodes that are not
currently allocated to a particular experiment, some or all of those nodes
are required in order to fulfill a reservation. Note that the admission control
system assumes that your experiment will last for the full default experiment
duration when making this calcuation. For example, if the default experiment
duration is 24 hours, and a large reservation will start in 10 hours, your
experiment may fail to be created due to the admission control system. If the
large reservation starts in 30 hours, you will be able to create the
experiment, but you may not be able to extend it.

Reservations can also prevent you from extending existing experiments, if that
extension would cause too few nodes to be available to satisfy a reservation. 
A message will appear on the experiment's status page warning you when this
situation will occur in the near future, and the reservation request dialog
will limit the length of reservation that you can request. If this happens, be
sure to save all of your work, as the administrators cannot grant extensions
that would interfere with reservations.

@section[#:tag "reservations-making"]{Making a Reservation}

To request a reservation, use the ``Reserve Nodes'' item from the ``Actions''
menu.

@screenshot["reservation-form.png"]

After filling out the number of and type of nodes and the time, the
@bold{check} button checks to see if the reservation is possible. If your
request is satisfiable, you will get a dialog box that lets you submit the
request.

@screenshot["reservation-submit.png"]

If your request is @italic{not} satisfiable, you will be given a chance to
modify the request and ``check'' again. In this case, the time when there will
not be enough nodes is shown, as will the number of nodes by which the request
exceeds availalbe resources. To make your reservation fit, try asking for a
different type of nodes, a smaller number, or a time further in the future.

Not all reservation requests are automatically accepted. Your request will be
shown as ``pending'' while it is being reviewed by the @(tb) administrators.
Requesting the smaller numbers of nodes, or for shorter periods of time, will
maximize the chances that youre request is accepted. Be sure to include
meaningful text in the ``Reason'' field, as administrators will use this to
determine whethr to grant your reservation.

You may have more than one reservation at a time; if you need resources of
more than one type, or on different clusters, you can get this by requesting
mutliple reservations.

@section[#:tag "reservations-using"]{Using a Reservation}

To use a reservation, simply create experiments as normal. Experiments run
during the duration of the reservation (even those begun before its start
time) are automatically counted towards the reservation. Experiments run
during reservations have expiration times as do normal experiments, so be sure
to extend them if necessary.

Since reservations are per-project, if you belong to more than one, make sure
to create the experiment under the correct project.

Experiments are not automatically terminated at the conclusion of a reservation
(though it may not be possible to extend them due to other reservations).
Remember to terminate your experiments when you are done with them, as you
would do normally.

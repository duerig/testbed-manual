"""An example of using parameters to construct a profile with a variable
number of nodes.

Instructions:
Wait for the profile instance to start, and then log in to one or more of
the VMs via the ssh port(s) specified below.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as rspec

# Describe the parameter(s) this profile script can accept.
portal.context.defineParameter( "n", "Number of VMs", portal.ParameterType.INTEGER, 1 )

# Retrieve the values the user specifies during instantiation.
params = portal.context.bindParameters()

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()

# Check parameter validity.
if params.n < 1 or params.n > 8:
    portal.context.reportError( portal.ParameterError( "You must choose at least 1 and no more than 8 VMs." ) )

for i in range( params.n ):
    # Create a XenVM and add it to the RSpec.
    node = request.XenVM( "node" + str( i ) )

# Print the RSpec to the enclosing page.
portal.context.printRequestRSpec()

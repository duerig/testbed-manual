"""An example of constructing a profile with install and execute services.
manually.

Instructions:
Wait for the profile instance to start, and then log in to the VM via the
ssh port specified below.  The install and execute services are handled
automatically during profile instantiation, with no manual intervention
required.
"""

import geni.portal as portal
import geni.rspec.pg as rspec

request = portal.context.makeRequestRSpec()

node1 = request.XenVM("node1")

# Specify the URL for the disk image
node1.disk_image = "<URL to disk image>"

# Install and execute scripts on the VM
node1.addService(rspec.Install(url="http://example.org/sample.tar.gz", path="/local"))
node1.addService(rspec.Execute(shell="bash", command="/local/example.sh"))

pc.printRequestRSpec()

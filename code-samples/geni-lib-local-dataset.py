"""This profile demonstrates how to mount a copy of a dataset in a file system. You can make changes to the dataset, but they are *temporary** (temporary means any changes will be lost when you terminate your experiment). You must explicitly request that the changes be saved if you want them to persist.

Instructions:
Log into your node, your dataset is mounted at `/mydata`.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as rspec
# Import the emulab extensions library.
import geni.rspec.emulab

# Create a portal context.
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Allocate a node and ask for a copy of a dataset to be mounted at /mydata
node = request.RawPC("node")
bs = node.Blockstore("bs", "/mydata")
# A demonstration dataset, there is nothing in the file system.
bs.dataset = "urn:publicid:IDN+emulab.net:testbed+imdataset+pgimdat"

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()

#!/usr/bin/python

"""
An example of a minimal OpenEPC profile.

Instructions:
Five nodes will be instantiated by this profile: three packet core
nodes, and two emulated RAN nodes (one eNodeB and one UE).  Please
refer to the OpenEPC documentation for instructions on how to
operate OpenEPC.
"""

# Import the standard RSpec and portal modules
import geni.portal as portal
import geni.rspec.pg as rspec

# Import phantoment-specific functionality
import geni.rspec.emulab.pnext as pn

request = portal.context.makeRequestRSpec()

# Create OpenEPC nodes; nodes made with this helper function must
# be explicitly added to request after creating them
epcen = pn.mkepcnode( "epc", pn.EPCROLES.ENABLERS )
request.addResource( epcen )

pgw = pn.mkepcnode( "pgw", pn.EPCROLES.PGW )
request.addResource( pgw )

sgw = pn.mkepcnode( "sgw", pn.EPCROLES.SGW_MME_SGSN )
request.addResource( sgw )

enodeb = pn.mkepcnode( "enodeb", pn.EPCROLES.ENODEB )
request.addResource( enodeb )

ue = pn.mkepcnode( "ue", pn.EPCROLES.CLIENT )
request.addResource( ue )

# Create LANs:
mgmt = request.EPClan( pn.EPCLANS.MGMT )
net_a = request.EPClan( pn.EPCLANS.NET_A )
net_b = request.EPClan( pn.EPCLANS.NET_B )
net_d = request.EPClan( pn.EPCLANS.NET_D )
an_lte = request.EPClan( pn.EPCLANS.AN_LTE )

# Add nodes to appropriate LANs:
mgmt.addMember( epcen )
mgmt.addMember( pgw )
mgmt.addMember( sgw )
mgmt.addMember( enodeb )
mgmt.addMember( ue )

net_a.addMember( epcen )
net_a.addMember( pgw )

net_b.addMember( pgw )
net_b.addMember( sgw )

net_d.addMember( sgw )
net_d.addMember( enodeb )

an_lte.addMember( enodeb )
an_lte.addMember( ue )

portal.context.printRequestRSpec()

"""An example of constructing a profile with two ARM64 nodes connected by a LAN.

Instructions:
Wait for the profile instance to start, and then log in to either host via the
ssh ports specified below.
"""

import geni.portal as portal
import geni.rspec.pg as rspec

request = portal.context.makeRequestRSpec()

# Create two raw "PC" nodes
node1 = request.RawPC("node1")
node2 = request.RawPC("node2")

# Set each of the two to specifically request "m400" nodes, which in CloudLab, are ARM
node1.hardware_type = "m400"
node2.hardware_type = "m400"

# Create interfaces for each node.
iface1 = node1.addInterface("if1")
iface2 = node2.addInterface("if2")

# Create a link with type LAN.
link = request.LAN("lan")

# Add both node interfaces to the link.
link.addInterface(iface1)
link.addInterface(iface2)

portal.context.printRequestRSpec()

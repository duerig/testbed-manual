#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "status-notes" #:version apt-version]{@(tb) Status Notes}

@(tb) is @bold{open}! The @seclink["hardware"]{hardware that is currently
deployed} represents about half of the Phase I deployment, and we are still
constantly adding @seclink["planned"]{new features}. That said, @(tb) is usable
in it's current state for getting ``real work'' done, so
@seclink["getting-started"]{give it a try!}

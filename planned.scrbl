#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "planned" #:version apt-version]{Planned Features}

This chapter describes features that are planned for @(tb) or under development:
please @seclink["getting-help"]{contact us} if you have any feedback or
suggestions!

@section[#:tag "planned-versioned-profiles"]{Versioned Profiles}

We plan to add the @italic{versioning} to profiles to capture the evolution
of a profile over time. When @seclink["updating-profiles"]{updating profiles},
the result will be a new version that does not (entirely) replace the profile
being updated.

There will be two types of versions: @italic{working} versions that should be
considered ephemeral, and @italic{published} versions that are intended to be
long-term stable. For example, a user may generate many working versions as
they refine their software, fix bugs, etc. Then, when the profile is in a state
where it is appropriate to share with others, it can be published.  Users will
be able to link to a specific version---example, to unambiguously identify which
version was used for a paper.

One limitation on this feature will be the fact that @(tb) has limited storage
space; we will have to apply a quota system that limits the amount of storage
that a project can use to store multiple versions of the same profile.

@section[#:tag "planned-persistent-storage"]{Persistent Storage}

For the time being, the contents of all disks in @(tb) are considered ephemeral:
the contents are lost whenever an experiment terminates. The only way to save
data is to copy it off or to @seclink["creating-profiles"]{create a profile}
using the disk.

We plan to change this by adding persistent storage that is hosted on storage
servers within @(tb). Users will be able to use the @(tb) web interface to create
and manage their persistent storage, and profiles will be able to reference
where these stores should be mounted in the experiment. When sharing profiles,
it will be possible to indicate that the persistent store may only be mounted
read-only---a common use case will be to put a dataset in a persistent store,
and allow other @(tb) users read-only access to the dataset.

There will be two types of persistent storage: @italic{block stores} which
are mounted using iSCSI, and which generally can only be mounted on one host
at a time, and @italic{file stores} which are mounted over NFS, and can be
mounted read/write by many nodes at the same time.

This feature will be based on Emulab's
@hyperlink["https://wiki.emulab.net/wiki/EmulabStorage"]{block storage system}.
Underlying this system is ZFS, which supports snapshots. We intend to expose
this snapshot functionality to users, and to allow profiles to mount specific
snapshots (eg. the version of a dataset used for a particular paper.)

It should be noted that performance of persistent stores will not be guaranteed
or isolated from other users, since it will be implemented using shared storage
servers that others may be accessing at the same time. Therefore, for
experiments whose @seclink["repeatable-research"]{repeatability} depends on 
I/O performance, all data should be copied to local disk before use.

@section[#:tag "planned-easier-profiles"]{Easier From-Scratch Profile Creation}

Currently, there are two ways to create profiles in @(tb): 
@seclink["creating-from-existing"]{cloning an existing profile} or creating one
from scratch by @seclink["creating-from-scratch"]{writing an RSpec by hand}.
We plan to add two more: a GUI for RSpec creation, and bindings to a programming
language for generation of RSpecs.

The GUI will be based on Jacks, an embeddable RSpec editor currently in
development for the GENI project. Jacks is already used in @(tb) to display
topologies in the profile selection dialog and on the experiment page. 

The programming language bindings will allow users to write programs in an
existing, popular language (likely Python) to create RSpecs. This will allow
users to use loops, conditionals, and other programming language constructs to
create large, complicated RSpecs. We are evaluating
@hyperlink["https://bitbucket.org/barnstorm/geni-lib"]{@code{geni-lib}} for
this purpose.

@section[#:tag "planned-quick-profiles"]{``Quick'' Profiles}

Sometimes, you just need one node running a particular
@seclink["disk-images"]{disk image}, without making a complicated profile to go
with it. We plan to add a ``quick profile'' feature that will create a one-off
experiment with a single node.

@apt-only{
    @section[#:tag "planned-virt-switching"]{Simpler Virtual/Physical Profile Switching}

    As recommended in the chapter about
    @seclink["repeatable-research"]{repeatable research}, moving back and forth
    between @seclink["virtual-machines"]{virtual} and
    @seclink["physical-machines"]{physical} resources is a good way to move
    from doing preliminary work to gathering final results for publication.
    Doing this currently requires making two profiles. We plan to make changes
    to the @(tb) interface to allow users to select, at experiment creation
    time, whether to use virtual or physical resources.
}

@section[#:tag "planned-physical-display"]{Improved Physical Resource Descriptions}

As part of the process of reserving resources on @(tb), a type of
@seclink["rspecs"]{RSpec} called a
@hyperlink["http://groups.geni.net/geni/wiki/GENIExperimenter/RSpecs#ManifestRSpec"]{manifest}
is created. The manifest gives a detailed description of the hardware
allocated, including the specifics of network topology. Currently, @(tb) does
not directly export this information to the user. In the interest of improving
transparency and @seclink["repeatable-research"]{repeatable research}, @(tb)
will develop interfaces to expose, explore, and export this information.

@clab-only{
    @section[#:tag "planned-bare-metal-switches"]{Bare-metal Access to Switches}

    Today, switches in @(tb) are treated as infrastructure; that is, they
    are under @(tb)'s control and, while we provide a high degree of
    transparency, we do not let users control them directly. We plan to
    make at least some switches---in most cases, ToRs, and in others,
    spine and/or core---directly accessible to users. This means that
    users will have console access to the switches, and will be able to
    reconfigure them and/or load different versions of firmware on them.
    (This will only be offered on switches that are only used by a single
    experiment at a time.)
}

@clab-only{
    @section[#:tag "planned-openflow"]{OpenFlow Support}

    All switches in @(tb) will be OpenFlow-capable. In the case of
    exclusive-access @seclink["planned-bare-metal-switches"]{bare metal
    switches}, users will get direct and complete OpenFlow access to the
    switches.  In the case of shared switches, we are investigating the use of
    @hyperlink["http://globalnoc.iu.edu/sdn/fsfw.html"]{FlowSpace Firewall}
    from the @hyperlink["http://globalnoc.iu.edu"]{GRNOC} and
    @hyperlink["http://www.internet2.edu"]{Internet2} for virtualization.
}

@clab-only{
    @section[#:tag "planned-switch-monitoring"]{Switch Monitoring}

    We plan to export many of the monitoring features available in 
    @(tb)'s infrastructure switches---port counters, queue lengths, etc.
}

@clab-only{
    @section[#:tag "planned-power-monitoring"]{Power Monitoring}

    Some of the equipment in @(tb) will have the ability to take fine-grained
    measurements of power usage and other environmental sensors (such as
    temperature). @(tb) will provide both logged and real-time access to this
    data for experimenters.
}

